﻿var showLogs = true;

if (showLogs) {
    console.log("Inside ScreenManager");
}
var gameWidth = 1312;//41 square
var gameHeight = 672; //21 square
var windowWidth = 800;
var windowHeight = 600;
var numRows = gameWidth / 32;
var numCols = gameHeight / 32;
var backgroundColor = "#000000";

var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'KulturGameDev', {
    preload: preload, create: create, update: update
});

// music //
//var mainMusic;
//var powerUpMusic;
//var plantBombMusic;
//var bombExplosionMusic;
var gameTime;
var previousTime = 0;
var previousTime2 = 0;
var GameLevel;
//var levelPortal;
var stageNumber=1;
var Enemy;
var score = 0;
var life = 3;

var blackScreen;
var blackScreenText;

var NUMBER_OF_BRICKS = 220;
var GplayerSpeed;
var GflameLevel;
var GbombNumber;

var GplayerSpeed2;
var GflameLevel2;
var GbombNumber2;
//var flameNumber = (flameLevel * flameLevel) + 1; // lvl 2 = 5 flame, lvl 3 = 9 flame
var keys;
var bombKey;
//var bomb;
//var bomB;
//////var flamePowerUp;
//////var bombPowerUp;
//////var speedPowerUp;
var bombBlocks;
var isExploded = true;
var isBombHitBrick = false;
//var flame;
var count = 0;
var flameBlocks;
var flameBlocks2;
//var flameHitDirection = [false, false, false, false] // yukarı, aşağı ,sağa, sola

var character;
var player2;

//var solidBlocks;
//var brickBloks;
//player 1 keys
var upKey;
var downKey;
var leftKey;
var rightKey;
// player 2 keys
var upKey2;
var downKey2;
var leftKey2;
var rightKey2;
var bombKey2;

var p2Bombindicator;
var p2Lifeindicator;
var p2Flameindicator;
var lifeText2;
var bombText2;
var flameText2;
var scoreText2;
var score2 = 0;
var life2 = 3;
// INDICATORS
var p1Bombindicator;
var p1Lifeindicator;
var p1Flameindicator;
var lifeText;
var bombText;
var flameText;
var scoreText;

function preload() {
    if (showLogs) {
        console.log("ScreenManager Preload");
    }
    // black screen
    game.load.image("blackScreen", "Pictures/Blocks/black_screen.png");
    // backgroun tile
    game.load.image("backgroundTile", "Pictures/Blocks/BackgroundTile_32px.png");
    // solid bricks
    game.load.image("solidBrick", "Pictures/Blocks/SolidBlock_32px.png");
    // all bomberman moves
    game.load.atlas("BomberMan_Moves", "Sprites/BomberManSprites.png", "Sprites/BomberManSprites.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    // Player Two moves
    game.load.atlas("Player2_Moves", "Sprites/PlayerTwoSprites.png", "Sprites/BomberManSprites.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    // sağa doğru animasyon
    game.load.atlas("playerWalkingRight", "Sprites/PlayerRightSprite.png", "Sprites/PlayerRightSprite.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    //aşağı doğru animasyon
    game.load.atlas("playerWalkingDown", "Sprites/PlayerDownSprite.png", "Sprites/PlayerDownSprite.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    // bomba patlama animasyonu
    game.load.atlas("bombAnim", "Sprites/BombSprite.png", "Sprites/BombSprite.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    // flame animasyonu
    //game.load.atlas("flameAnim", "Sprites/FlameSprite.png", "Sprites/FlameSprite.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    game.load.atlas("flameAnim", "Sprites/fireBallSprite.png", "Sprites/fireBallSprite.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    // explodable brick
    game.load.image("explodableBrick", "Pictures/Blocks/brick_32px.png");
    // bomb powerup
    game.load.image("boomb", "Pictures/PowerUps/BombPowerup.png");
    // flame powerup
    game.load.image("flaame", "Pictures/PowerUps/FlamePowerup.png");
    // speed power up
    game.load.image("speed", "Pictures/PowerUps/SpeedPowerup.png");
    // level portal/gate
    //game.load.image("portal", "Pictures/Blocks/Portal_32px.png");
    game.load.image("portal", "Pictures/Blocks/Misc_Door.png");
    // load main music
    game.load.audio("mainMusic", "Audios/GameAudio/main.mp3");
    // load powerUp music
    game.load.audio("powerUpMusic", "Audios/PowerUpAudio/power-up-clear.mp3");
    // load plant bomb music
    game.load.audio("plantBombMusic", "Audios/GameAudio/plantBomb2.wav");
    // load explosion music
    //game.load.audio("explosionBombMusic", "Audios/GameAudio/bombExplosion.wav");
    game.load.audio("explosionBombMusic", "Audios/GameAudio/BombSound.mp3");
    // player dead music
    game.load.audio("deadMusic","Audios/PlayerAudio/PlayerDead2.wav");
    // load level 1 enemy sprite
    game.load.atlas("enemy01moves", "Sprites/enemy01.png", "Sprites/enemy01.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    // load leve 2 enemy sprite
    game.load.atlas("enemy02moves", "Sprites/enemy2sprite.png", "Sprites/enemy2.json", Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    // load player 1 bomb indicator
    game.load.image("player1Bombindicator", "Pictures/Blocks/player1Bombindicator.png");
    // load player 1 life indicator
    game.load.image("player1Lifeindicator", "Pictures/Blocks/player1Lifeindicator.png");
    // load player 1-2 flame indicator
    game.load.image("Flameindicator", "Pictures/Blocks/flameIndicator.png");
    //load player 2 life indicator
    game.load.image("p2Lifeindicator", "Pictures/Blocks/p2Lifeindicator.png");
}
function create() {
    if (showLogs) {
        console.log("ScreenManager");
    }
  
    game.world.setBounds(0, 0, gameWidth, gameHeight);
    game.physics.startSystem(Phaser.Physics.ARCADE);   

    GameLevel = new GameObjects.InitializeGameLevel();
    GameLevel.init(stageNumber);

    flameBlocks = game.add.physicsGroup();
    flameBlocks2 = game.add.physicsGroup();
    bombBlocks = game.add.physicsGroup();
    
    GameLevel.ReloadMap();
    
    character = new GameObjects.Character();
    character.init("Bomberman", 48, 48, 'BomberMan_Moves');
    console.log("name " + character.GetName());
    //character.SavePlayer(GplayerSpeed, GflameLevel, GbombNumber);
    
    player2 = new GameObjects.Character();
    player2.init("Player2", 1264,624, "Player2_Moves");
    console.log("name " + player2.GetName());
    //player2.SavePlayer(GplayerSpeed2, GflameLevel2, GbombNumber2);

    savePlayerInventory();
    setUserGUI();

    // PLAYER 1 BUTTON CONFIG
    upKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
    downKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
    leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
    rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
 
    // Bombaları "Z" harfine basınca koysun
    //keys = game.input.keyboard.createCursorKeys();
    bombKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    bombKey.onDown.add(plantBomb, this);
   
    // PLAYER 2 BUTTON CONFIG
    upKey2 = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    downKey2 = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    leftKey2 = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    rightKey2 = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);

    bombKey2 = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_3);
    bombKey2.onDown.add(plantBomb2, this);

    
    /*blackScreen = game.add.image(0, 0, "blackScreen");
    blackScreen.width = gameWidth;
    blackScreen.height = gameHeight;
    var style = { font: "18px Cooper Black", fill: "#ffffff", align: "center" };
    blackScreenText = game.add.text(game.world.centerX, game.world.centerX, "Life : " + life + "    Score : " + score, style);
    game.time.events.add(Phaser.Timer.SECOND * 3,killBlackScreens,this);*/
}
function killBlackScreens() {
    blackScreen.kill();
    blackScreenText.kill();
}
function render() {
    game.debug.text('Selam', 32, 32);
}

function plantBomb() {       
    character.PlantBomb(flameBlocks);  
}
function plantBomb2() {
    player2.PlantBomb(flameBlocks2);
}


function update() {
    
    gameTime = game.time.totalElapsedSeconds();
    ////////////////// PLAYER 1 COLLISION DETECTION //////////////
    // character flame'in içine girerse ölür
    game.physics.arcade.collideSpriteVsGroup(character.GetSprite(), flameBlocks, collisionHandlerFlame, processHandler, this, true);
    game.physics.arcade.collideSpriteVsGroup(character.GetSprite(), flameBlocks2, collisionHandlerFlame, processHandler, this, true);
    // character direk flame'in içindeyse ölür
    game.physics.arcade.overlap(character.GetSprite(), flameBlocks, collisionHandlerFlame, processHandler, this);
    game.physics.arcade.overlap(character.GetSprite(), flameBlocks2, collisionHandlerFlame, processHandler, this);
    // character solidlerden geçemez
    game.physics.arcade.collide(character.GetSprite(), GameLevel.GetSolidBlocks(), collisionHandler, processHandler, this); //solidBlocks
    //// character bricklerden geçemez
    game.physics.arcade.collide(character.GetSprite(), GameLevel.GetBrickBlocks(), collisionHandlerBrick, processHandler, this); //brickBloks
    // char bombayı iter
    game.physics.arcade.collide(character.GetSprite(), bombBlocks, collidePlayerBomb, processHandler, this);
    // collide player with bomb power up
    game.physics.arcade.collide(character.GetSprite(), GameLevel.GetBombPowerUp(), increaseBomb, processHandler, this); //bombPowerUp
    // collide player with flamee power up
    game.physics.arcade.collide(character.GetSprite(), GameLevel.GetFlamePowerUp(), increaseFlame, processHandler, this); //flamePowerUp
    // collide player with speed power up
    game.physics.arcade.collide(character.GetSprite(), GameLevel.GetSpeedPowerUp(), increaseSpeed, processHandler, this); //speedPowerUp
    // collide player with speed power up
    game.physics.arcade.collide(character.GetSprite(), GameLevel.GetGateSprite(), changeGameLevel, processHandler, this); // levelPortal
    // collide player with enemies
    game.physics.arcade.collide(character.GetSprite(), GameLevel.GetEnemies(), collidePlayerEnemy, processHandler, this);
    //////////////////////////////////// PLAYER 2 COLLISION DETECTION ///////////////////
    // character flame'in içine girerse ölür
    game.physics.arcade.collideSpriteVsGroup(player2.GetSprite(), flameBlocks2, collisionHandlerFlameP2, processHandler, this, true);
    game.physics.arcade.collideSpriteVsGroup(player2.GetSprite(), flameBlocks, collisionHandlerFlameP2, processHandler, this, true);
    // character direk flame'in içindeyse ölür
    game.physics.arcade.overlap(player2.GetSprite(), flameBlocks2, collisionHandlerFlameP2, processHandler, this);
    game.physics.arcade.overlap(player2.GetSprite(), flameBlocks, collisionHandlerFlameP2, processHandler, this);

    // character solidlerden geçemez
    game.physics.arcade.collide(player2.GetSprite(), GameLevel.GetSolidBlocks(), collisionHandler, processHandler, this); //solidBlocks
    //// character bricklerden geçemez
    game.physics.arcade.collide(player2.GetSprite(), GameLevel.GetBrickBlocks(), collisionHandlerBrick, processHandler, this); //brickBloks
    // char bombayı iter
    game.physics.arcade.collide(player2.GetSprite(), bombBlocks, collidePlayerBomb, processHandler, this);
    // collide player with bomb power up
    game.physics.arcade.collide(player2.GetSprite(), GameLevel.GetBombPowerUp(), increaseBomb2, processHandler, this); //bombPowerUp
    // collide player with flamee power up
    game.physics.arcade.collide(player2.GetSprite(), GameLevel.GetFlamePowerUp(), increaseFlame2, processHandler, this); //flamePowerUp
    // collide player with speed power up
    game.physics.arcade.collide(player2.GetSprite(), GameLevel.GetSpeedPowerUp(), increaseSpeed2, processHandler, this); //speedPowerUp
    // collide player with speed power up
    game.physics.arcade.collide(player2.GetSprite(), GameLevel.GetGateSprite(), changeGameLevel, processHandler, this); // levelPortal
    // collide player with enemies
    game.physics.arcade.collide(player2.GetSprite(), GameLevel.GetEnemies(), collidePlayer2Enemy, processHandler, this);

    // flameler brickleri patlatır (flameBlocks)
    game.physics.arcade.collideGroupVsGroup(flameBlocks, GameLevel.GetBrickBlocks(), collideFlameBrick, processHandler, this, true); //brickBloks
    game.physics.arcade.collideGroupVsGroup(flameBlocks2, GameLevel.GetBrickBlocks(), collideFlameBrick2, processHandler, this, true); //brickBloks
    // kayan bomba brick'e çarpınca durur.
    game.physics.arcade.collide(bombBlocks, GameLevel.GetBrickBlocks(), collideBombBrick, processHandler, this);
    // kayan bomba brick'e çarpınca durur.
    game.physics.arcade.collide(bombBlocks, GameLevel.GetSolidBlocks(), collideBombBrick, processHandler, this);
    // collide bombs itselves
    game.physics.arcade.collideGroupVsSelf(bombBlocks, collideBombAndBomb, processHandler, this);
    // collide environment with enemies
    game.physics.arcade.collideGroupVsGroup(GameLevel.GetBrickBlocks(), GameLevel.GetEnemies(), collideEnvironmentEnemy, processHandler, this);
    game.physics.arcade.collideGroupVsGroup(GameLevel.GetSolidBlocks(), GameLevel.GetEnemies(), collideEnvironmentEnemy, processHandler, this);
    game.physics.arcade.collideGroupVsGroup(bombBlocks, GameLevel.GetEnemies(), collideEnvironmentEnemy, processHandler, this);
    /// flames collide enemies
    game.physics.arcade.collideGroupVsGroup(flameBlocks, GameLevel.GetEnemies(), collideFlameEnemy, processHandler, this,true);
    game.physics.arcade.collideGroupVsGroup(flameBlocks2, GameLevel.GetEnemies(), collideFlameEnemy2, processHandler, this, true);

    // düşman direk flame'in içindeyse ölür
    //game.physics.arcade.overlap(GameLevel.GetEnemies(), flameBlocks, collideFlameEnemy, processHandler, this);


    updateUserGUI();
    // save character's power ups
    //character.SavePlayer(GplayerSpeed, GflameLevel, GbombNumber);
    //player2.SavePlayer(GplayerSpeed2, GflameLevel2, GbombNumber2);
    savePlayerInventory();

    character.Stand();
    player2.Stand();

    //// PLAYER 1 KEY LISTENER
    if (upKey.isDown) {
        //console.log("up");
        character.MoveUp();
    }
    else if (downKey.isDown) {
        //console.log("down");
        character.MoveDown();
    }

    if (leftKey.isDown) {
        //console.log("left");
        character.MoveLeft();
    }
    else if (rightKey.isDown) {
        //console.log("right");
        character.MoveRight();
    }

    // PLAYER 2 KEY LISTENER
    if (upKey2.isDown) {
        //console.log("up");
        player2.MoveUp();
    }
    else if (downKey2.isDown) {
        //console.log("down");
        player2.MoveDown();
    }

    if (leftKey2.isDown) {
        //console.log("left");
        player2.MoveLeft();
    }
    else if (rightKey2.isDown) {
        //console.log("right");
        player2.MoveRight();
    }

    //if (keys.up.isDown ) {
    //    //console.log("up");
    //    character.MoveUp();              
    //}
    //else if (keys.down.isDown ) {
    //    //console.log("down");
    //    character.MoveDown();      
    //} 

    //if (keys.left.isDown ) {
    //    //console.log("left");
    //    character.MoveLeft();     
    //}
    //else if (keys.right.isDown ) {
    //    //console.log("right");
    //    character.MoveRight();     
    //}
    
}

function processHandler(player, veg) {
    return true;
}
function collisionHandler(player, veg) {
    //player.body.velocity.x = 0;
    //player.body.velocity.y = 0;
    console.log("velocity absorbed.");
}
function collisionHandlerBrick(player, veg) {
    //player.body.velocity.x = 0;
    //player.body.velocity.y = 0;
    //veg.kill();
    console.log("Brick !!");
}
function collisionHandlerFlame(player, veg) {
    if (gameTime > previousTime + 4) {
        if(life>0)
            life--;

        GameLevel.playPlayerDeadMusic();
        console.log("Player 1 Died!!!!!!!!!!!!");
        character.Dead(48, 48);
        //player.position.setTo(48, 48);
        previousTime = gameTime;

        checkPlayersLives();
    }
}
function collisionHandlerFlameP2(player, veg) {
    if (gameTime > previousTime2 + 4) {
        if (life2 > 0)
        life2--;
        GameLevel.playPlayerDeadMusic();
        console.log("Player 2 Died!!!!!!!!!!!!");
        player2.Dead(1264, 624);
        //player.position.setTo(1264, 624);      
        previousTime2 = gameTime;

        checkPlayersLives();
    }
}

function collideFlameBrick(flame, brick) {
    var i = Math.floor(brick.position.x / 32);
    var j = Math.floor(brick.position.y / 32);
    //console.log("brick position: i:" + i + " j: " + j);
   
    brick.kill();
    //UPDATE EXPLODED BRICK PLACES AS EMPTY
    GameLevel.UpdateArea(i, j, 0);

    score += 100;
    console.log("Flame touched to brick");
}

function collideFlameBrick2(flame, brick) {
    var i = Math.floor(brick.position.x / 32);
    var j = Math.floor(brick.position.y / 32);
    //console.log("brick position: i:" + i + " j: " + j);

    brick.kill();
    //UPDATE EXPLODED BRICK PLACES AS EMPTY
    GameLevel.UpdateArea(i, j, 0);

    score2 += 100;
    console.log("Flame touched to brick");
}

function collidePlayerBomb(player, bomb) {
    //player.body.velocity.x = 0;
    //player.body.velocity.y = 0;
    console.log("Bomb absorbed.");
}
function collideBombBrick(bomba, bricks) {
    bomba.body.velocity.x = 0;
    bomba.body.velocity.y = 0;
}
function collideBombAndBomb(bomb1,bomb2){
    console.log("Bombs are collided");
}

function increaseBomb(player,powerup) {
    console.log("player 1 gained bomb powerUp");
    character.AddBomb();
    GameLevel.playPowerUpMusic();
    powerup.kill();
}
function increaseBomb2(player, powerup) {
    console.log("player 2 gained bomb powerUp");
    player2.AddBomb();
    GameLevel.playPowerUpMusic();
    powerup.kill();
}

function increaseFlame(player,powerup) {
    console.log("player 1 gained flame power up");
    character.AddFlame();
    GameLevel.playPowerUpMusic();
    powerup.kill();
}
function increaseFlame2(player, powerup) {
    console.log("player 2 gained flame power up");
    player2.AddFlame();
    GameLevel.playPowerUpMusic();
    powerup.kill();
}

function increaseSpeed(player, powerup) {
    console.log("player 1  gained speed power up");
    character.AddSpeed();
    GameLevel.playPowerUpMusic();
    powerup.kill();
}
function increaseSpeed2(player, powerup) {
    console.log("player 2  gained speed power up");
    player2.AddSpeed();
    GameLevel.playPowerUpMusic();
    powerup.kill();
}

function changeGameLevel(player,gate) {
    console.log("Going Next Level !!");
    gate.kill();
    //mainMusic.stop();
    //powerUpMusic.stop();
    //powerUpMusic.play("enterPortal");
    GameLevel.playEndOfGameMusic();
    stageNumber++;
    gameChangeScreen();
    //setLevelSpecs();
}

function gameChangeScreen() {
    blackScreen = game.add.image(0, 0, "blackScreen");
    blackScreen.width = gameWidth;
    blackScreen.height = gameHeight;
    var style = { font: "18px Cooper Black", fill: "#ffffff", align: "center" };
    blackScreenText = game.add.text(game.world.centerX - 350, game.world.centerY - 100, "Life : " + life + "    Score : " + score +
                                        "\n\tStage: "+stageNumber, style);
    blackScreenText.fixedToCamera = true;
    game.time.events.add(Phaser.Timer.SECOND * 4, setLevelSpecs, this);
}

function setLevelSpecs() {
    //GameLevel = new GameObjects.InitializeGameLevel();
    //GameLevel.init(stageNumber);
    killBlackScreens();

    GameLevel.ReloadMap();

    character.reloadPlayer(48,48,GplayerSpeed,GbombNumber,GflameLevel);
    player2.reloadPlayer(1264, 624,GplayerSpeed2,GbombNumber2,GflameLevel2);
    killUI();
    setUserGUI();
}

function collidePlayerEnemy(player, enemy) {
    if (gameTime > previousTime + 4) {
        if (life > 0)
            life--;

        GameLevel.playPlayerDeadMusic();
        character.Dead(48, 48);
        //player.position.setTo(48, 48);
        console.log("Collided with enemy!!!!!");
        previousTime = gameTime;

        checkPlayersLives();
    }
}

function collidePlayer2Enemy(player, enemy) {
    if (gameTime > previousTime + 4) {
        if (life2 > 0)
            life2--;

        GameLevel.playPlayerDeadMusic();
        player2.Dead(1264, 624);
        //player.position.setTo(1264, 624);
        console.log("Collided with enemy!!!!!");
        previousTime = gameTime;

        checkPlayersLives();
    }
}

function collideEnvironmentEnemy(block,enemy) {
    if (enemy.body.touching.left) {
        //console.log("touching left of enemy!!!!");
        enemy.body.velocity.x = 0;
        enemy.body.velocity.y = -50; // sola çarpınca yukarı dön
    }
    else if (enemy.body.touching.right) {
        //console.log("touching right of enemy!!!!");
        enemy.body.velocity.x = 0;
        enemy.body.velocity.y = 50; // sağa çarpınca aşağı dön
    }
    else if (enemy.body.touching.up) {
        //console.log("touching up of enemy!!!!");
        enemy.body.velocity.y = 0;
        enemy.body.velocity.x = 50; // yukaır çarpınca saga dön
    }
    else if (enemy.body.touching.down) {
        //console.log("touching down of enemy!!!!");
        enemy.body.velocity.y = 0;
        enemy.body.velocity.x = -50; // aşağı çarpınca sola dön
    }
}

function collideFlameEnemy(flame,enemy) {
    console.log("enemy killed!!");
    score += 500;
    enemy.destroy();
}

function collideFlameEnemy2(flame, enemy) {
    console.log("enemy killed!!");
    score2 += 500;
    enemy.destroy();
}

function savePlayerInventory() {
    GplayerSpeed = character.GetSpeed();
    GflameLevel = character.GetFlameLevel();
    GbombNumber = character.GetBombNumber();

    GplayerSpeed2 = player2.GetSpeed();
    GflameLevel2 = player2.GetFlameLevel();
    GbombNumber2 = player2.GetBombNumber();
}

function setUserGUI() {
    placePlayersIndicators();

    var style = { font: "18px Cooper Black", fill: "#ffffff", align: "center" };
    lifeText = game.add.text(115, 16, "x " + life,style);
    bombText = game.add.text(210, 16, "x " + character.GetBombNumber(), style);
    flameText = game.add.text(310, 16, "x " + character.GetFlameLevel(), style);
    scoreText = game.add.text(400, 16, "Score: " +score, style);
    lifeText.anchor.setTo(.5, .5);
    bombText.anchor.setTo(.5, .5);
    flameText.anchor.setTo(.5, .5);
    scoreText.anchor.setTo(.5, .5);
    scoreText.fixedToCamera = true;

    lifeText2 = game.add.text(851, 16, "x " + life2, style);
    bombText2 = game.add.text(947, 16, "x " + player2.GetBombNumber(), style);
    flameText2 = game.add.text(1043, 16, "x " + player2.GetFlameLevel(), style);
    scoreText2 = game.add.text(1150, 16, "Score: " + score2, style);
    lifeText2.anchor.setTo(.5, .5);
    bombText2.anchor.setTo(.5, .5);
    flameText2.anchor.setTo(.5, .5);
    scoreText2.anchor.setTo(.5, .5);
}

function updateUserGUI() {
    // update text
    lifeText.setText("x " + life);
    bombText.setText("x " + character.GetBombNumber());
    flameText.setText("x "+ character.GetFlameLevel());
    scoreText.setText("Score: " + score);
    //("Bomb: " + character.GetBombNumber() + "   Flame: " + character.GetFlameLevel() + "  Life : " + life + "    Score : " + score);
    lifeText2.setText("x " + life2);
    bombText2.setText("x " + player2.GetBombNumber());
    flameText2.setText("x " + player2.GetFlameLevel());
    scoreText2.setText("Score: " + score2);
}

function placePlayersIndicators() {
    p1Lifeindicator = game.add.sprite(80, 16, "player1Lifeindicator");
    p1Lifeindicator.anchor.setTo(.5, .5);

    p1Bombindicator = game.add.sprite(176, 16, "player1Bombindicator");
    p1Bombindicator.anchor.setTo(.5, .5);

    p1Flameindicator = game.add.sprite(272, 16, "Flameindicator");
    p1Flameindicator.anchor.setTo(.5, .5);

    p2Lifeindicator = game.add.sprite(816, 16, "p2Lifeindicator");
    p2Lifeindicator.anchor.setTo(.5, .5);

    p2Bombindicator = game.add.sprite(912, 16, "player1Bombindicator");
    p2Bombindicator.anchor.setTo(.5, .5);

    p2Flameindicator = game.add.sprite(1008, 16, "Flameindicator");
    p2Flameindicator.anchor.setTo(.5, .5);
}
function killUI() {
    lifeText.kill();
    bombText.kill();
    flameText.kill();
    lifeText2.kill();
    bombText2.kill();
    flameText2.kill();
    scoreText2.kill();

    p1Lifeindicator.kill();
    p1Bombindicator.kill();
    p1Flameindicator.kill();
    p2Lifeindicator.kill();
    p2Bombindicator.kill();
    p2Flameindicator.kill();
}

function clearEndGameUI(sprite,text) {
    sprite.kill();
    text.kill();
    GameLevel.playEndOfGameMusic();
    gameChangeScreen();
}

function checkPlayersLives() {
    if (life <= 0) {
        var style = { font: "36px Cooper Black", fill: "#ffffff", align: "center" };
        var endGame = game.add.text((gameWidth / 2) - 200, (gameHeight / 2) - 200, "Player Two Won!", style);

        var p2Lifeindicator = game.add.sprite((gameWidth / 2) - 50, (gameHeight / 2) - 50, "p2Lifeindicator");
        p2Lifeindicator.scale.x = 5;
        p2Lifeindicator.scale.y = 5;
        p2Lifeindicator.anchor.setTo(.5, .5);
        game.time.events.add(Phaser.Timer.SECOND * 10, function () { clearEndGameUI(p2Lifeindicator, endGame) }, this);
        life = 3;
		life2 = 3;
        //endGame.kill();
    } else if (life2 <= 0) {
        var style = { font: "36px Cooper Black", fill: "#ffffff", align: "center" };
        var endGame = game.add.text((gameWidth / 2) - 200, (gameHeight / 2) - 200, "Player One Won!", style);

        var p1Lifeindicator = game.add.sprite((gameWidth / 2) - 50, (gameHeight / 2) - 50, "player1Lifeindicator");
        p1Lifeindicator.scale.x = 5;
        p1Lifeindicator.scale.y = 5;
        p1Lifeindicator.anchor.setTo(.5, .5);
        game.time.events.add(Phaser.Timer.SECOND * 10, function () { clearEndGameUI(p1Lifeindicator, endGame) }, this);
        life2 = 3;
		 life = 3;
        //endGame.kill();
    }
}