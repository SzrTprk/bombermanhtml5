﻿var GameObjects = window.GameObjects || {};

GameObjects.Character = (function () {
    return {
        sprite: "",
        bFacingRight: "",
        Name: "",
        playerSpeed: "",
        flamelvl: "",
        bombNumber: "",
        isDead: "",
        init: function (CharName, x, y, spriteName) {
            this.isDead = false;
            this.bFacingRight = true;
            this.Name = CharName;
            this.flamelvl = 2;
            this.bombNumber = 2;
            this.playerSpeed = 75;
            this.sprite = game.add.sprite(x, y, spriteName);
            game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
            this.sprite.body.collideWorldBounds = true;
            this.sprite.animations.add('walkRL', [16, 17, 18, 19, 20, 21, 22, 23]);
            this.sprite.animations.add('walkUp', [0, 1, 2, 3, 4, 5, 6, 7]);
            this.sprite.animations.add('walkDown', [8, 9, 10, 11, 12, 13, 14, 15]);
            this.sprite.animations.add('deadPlayer', [24, 25, 26, 27]);
            this.sprite.animations.add('stand', [8]);
            this.sprite.animations.play('stand', 8, true);
            this.sprite.anchor.setTo(.5, .5);

            game.camera.follow(this.sprite);
        },
        reloadPlayer: function (x, y, speed, bombCount, flameLvl) {
            this.playerSpeed = speed;
            this.bombNumber = bombCount;
            this.flamelvl = flameLvl;

            this.sprite.position.setTo(x, y);
        },
        //SavePlayer: function (GpSpeed, GfLevel, GbNumber) {
        //    GpSpeed = this.flamelvl;
        //    GfLevel = this.bombNumber;
        //    GbNumber = this.playerSpeed;
        //},
        AddFlame: function () {
            this.flamelvl++;
        },
        AddBomb: function () {
            this.bombNumber++;
        },
        DeleteBomb: function () {
            this.bombNumber--;
        },
        AddSpeed: function () {
            this.playerSpeed += 25;
        },
        GetFlameLevel: function () {
            return this.flamelvl;
        },
        GetBombNumber: function () {
            return this.bombNumber;
        },
        GetSpeed: function () {
            return this.playerSpeed;
        },
        GetName: function () {
            return this.Name;
        },
        GetSprite: function () {
            return this.sprite;
        },
        MoveRight: function () {

            if (!this.isDead) {
                if (!this.bFacingRight) {
                    this.sprite.scale.x *= -1;
                    this.bFacingRight = true;
                }
                this.sprite.body.velocity.x = this.playerSpeed;
                this.sprite.animations.play("walkRL", 32, false);
                console.log("right");
            }
        },
        MoveLeft: function () {

            if (!this.isDead) {
                if (this.bFacingRight) {
                    this.sprite.scale.x *= -1;
                    this.bFacingRight = false;
                }
                this.sprite.body.velocity.x = -1 * this.playerSpeed;
                this.sprite.animations.play("walkRL", 32, false);
                console.log("left");
            }
        },
        MoveUp: function () {
            if (!this.isDead) {
                this.sprite.body.velocity.y = -1 * this.playerSpeed;
                this.sprite.animations.play("walkUp", 32, false);
                console.log("up");
            }
        },
        MoveDown: function () {
            if (!this.isDead) {
                this.sprite.body.velocity.y = this.playerSpeed;
                this.sprite.animations.play("walkDown", 32, false);
                console.log("down");
            }
        },
        Stand: function () {
            //sprite.animations.play("stand", 8, true);
            this.sprite.body.velocity.x = 0;
            this.sprite.body.velocity.y = 0;
            //console.log("stand");
        },
        Dead: function (x, y) {
            if (!this.isDead) {
                this.isDead = true;
                this.sprite.animations.play("deadPlayer", 4, false);
                this.sprite.animations.currentAnim.onComplete.add(function () {
                    this.sprite.animations.play('stand', 8, true);
                    this.sprite.position.setTo(x, y);
                    this.isDead = false;
                }, this);
                 console.log("Deead animation plays for: "+this.Name);
            }
        },
        PlantBomb: function (flameBlocks) {
            if (!this.isDead) {
                if (this.bombNumber > 0) {
                    console.log("Bomb planted !!");
                    var bomb = new GameObjects.Bomb();
                    var spx = this.sprite.position.x
                    var spy = this.sprite.position.y;
                    // bombayı tam boşluğa ortalanmış şekilde koyması için
                    spx = (Math.floor(spx / 32, 3)) * 32 + 16;
                    spy = (Math.floor(spy / 32, 3)) * 32 + 16;
                    //console.log("Bomb position: spx:" + spx + " spy: " + spy);
                    this.bombNumber--;
                    // increase available bomb number
                    game.time.events.add(Phaser.Timer.SECOND * 3, this.AddBomb, this);
                    // put the bomb
                    bomb.init(spx, spy, this.flamelvl, this.bombNumber,flameBlocks);
                }
            }
        }
    };
});

GameObjects.Flame = (function () {
    return {
        sprite: "",
        Name: "",
        //flame:"",
        //flameBlocks:"",
        init: function (posX, posY, flameBlocks, num) {
            this.Name = "Flame";
            //flameBlocks = game.add.physicsGroup();
            var flame = flameBlocks.create(posX, posY, "flameAnim");
            flame.body.immovable = true;
            flame.animations.add("triggerFlame");
            flame.anchor.setTo(.5, .5);
            flame.animations.play("triggerFlame", 8, false);
            flame.animations.currentAnim.onComplete.add(function () {
                console.log(num + ". flames extinguished");
                flame.kill();
            }, this);
        },
    };
});

GameObjects.Bomb = (function () {
    return {
        sprite: "",
        Name: "",
        //bombBlocks: "",
        pos_x: "",
        pos_y: "",
        //bomb: "",
        flameLVL: "",
        bombMusics: "",
        area: "",
        //isExploded: true,
        init: function (posX, posY, fLevel, bombNumber, flameBlocks) {
            this.Name = "Bomb";
            this.flameLVL = fLevel;
            this.pos_x = posX;
            this.pos_y = posY;
            //GET WORLD AREA INFO
            this.area = GameLevel.GetArea();
            // INITALIZE BOMB FLAME DIRECTION
            var flameHitDirection = [false, false, false, false] // yukarı, aşağı ,sağa, sola

            this.bombMusics = new GameObjects.Sounds();
            this.bombMusics.init();

            var bomb = bombBlocks.create(posX, posY, "bombAnim");
            bomb.body.immovable = true;
            bomb.animations.add("putBomb");
            bomb.anchor.setTo(.5, .5);
            //game.physics.arcade.enable(bomb, Phaser.Physics.ARCADE);
            bomb.body.collideWorldBounds = true;
            bomb.animations.play("putBomb", 3, true);

            // SET BOMB FLAME DIRECTION
            // check around of the bomb if there are bricks or solid bricks
            var i = Math.floor(this.pos_x / 32);
            var j = Math.floor(this.pos_y / 32);
            console.log("Bomb position: i:" + i + " j: " + j);
            if (this.area[i + 1][j] == 1 || this.area[i + 1][j] == 2) {
                console.log("Right area is Brick or Solid");
                flameHitDirection[2] = true; // sağda
            }
            if (this.area[i - 1][j] == 1 || this.area[i - 1][j] == 2) {
                console.log("Left area is Brick or Solid");
                flameHitDirection[3] = true; // solda
            }
            if (this.area[i][j + 1] == 1 || this.area[i][j + 1] == 2) {
                console.log("Down area is Brick or Solid");
                flameHitDirection[1] = true; // aşağıda
            }
            if (this.area[i][j - 1] == 1 || this.area[i][j - 1] == 2) {
                console.log("Up area is Brick or Solid");
                flameHitDirection[0] = true; // yukarıda
            }

            /////// play plant bomb music
            this.bombMusics.playPlantBombMusic();
            game.time.events.add(Phaser.Timer.SECOND * 3, function () { this.blowBomb(bomb, bombNumber, flameHitDirection, flameBlocks); }, this);
        },

        GetX: function () {
            return this.pos_x;
        },
        GetY: function () {
            return this.pos_y;
        },
        blowBomb: function (bomb, bombNumber, flameHitDirection, flameBlocks) {
            console.log("Bomb: " + bombNumber + " is exploded");

            bomb.kill();
            ////// bomb explosion music
            this.bombMusics.playBombExplosionMusic();

            console.log("Flames fire up !!");
            for (var i = 0 ; i < this.flameLVL ; i++, count++) {
                if (i > 0) {
                    // sağa
                    if (!flameHitDirection[2] || i < 2) {
                        var f2 = new GameObjects.Flame();
                        f2.init(bomb.x + (32 * i), bomb.y, flameBlocks, 2);
                    }
                    // sola
                    if (!flameHitDirection[3] || i < 2) {
                        var f3 = new GameObjects.Flame();
                        f3.init(bomb.x - (32 * i), bomb.y, flameBlocks, 3);
                    }
                }
                else {
                    var f1 = new GameObjects.Flame();
                    f1.init(bomb.x, bomb.y, flameBlocks, 1);
                }
            }
            for (var j = 0 ; j < this.flameLVL ; j++, count++) {
                if (j > 0) {
                    // aşağısı
                    if (!flameHitDirection[1] || j < 2) {
                        var f4 = new GameObjects.Flame();
                        f4.init(bomb.x, bomb.y + (32 * j), flameBlocks, 4);
                    }
                    // yukarısı
                    if (!flameHitDirection[0] || j < 2) {
                        var f5 = new GameObjects.Flame();
                        f5.init(bomb.x, bomb.y - (32 * j), flameBlocks, 5);
                    }

                }
            }
        }
    };
});

GameObjects.InitializeGameLevel = (function () {
    return {
        numberOfEnemies: "",
        enemyBlocks: "",
        brickBloks: "",
        solidBlocks: "",
        flamePowerUp: "",
        bombPowerUp: "",
        speedPowerUp: "",
        levelPortal: "",
        Empty: "",
        //Area:"",
        gameMusic: "",
        Area: "",
        isInitialized: false,
        init: function (stageNumber) {
            this.numberOfEnemies = 6;
            Empty = 0, Brick = 1, Rock = 2, Pbomb = 3, Pflame = 4, Pspeed = 5, PowerUp = 6;

            var tiles = [];
            for (var x = 0; x < numRows; x++) {
                tiles[x] = [];
                for (var y = 0; y < numCols; y++) {
                    tiles[x][y] = Empty;
                }
            }

            for (var i = 0 ; i < numRows ; i++) {
                for (var j = 0; j < numCols; j++) {
                    tiles[i][j] = game.add.sprite(i * 32, j * 32, 'backgroundTile');
                }
            }
            game.physics.enable(tiles);

            this.gameMusic = new GameObjects.Sounds();
            this.gameMusic.init();

            //this.LoadDynamicItems();

        },
        GetBrickBlocks: function () {
            return this.brickBloks;
        },
        GetSolidBlocks: function () {
            return this.solidBlocks;
        },
        GetFlamePowerUp: function () {
            return this.flamePowerUp;
        },
        GetSpeedPowerUp: function () {
            return this.speedPowerUp;
        },
        GetBombPowerUp: function () {
            return this.bombPowerUp;
        },
        GetGateSprite: function () {
            return this.levelPortal;
        },
        LoadDynamicItems: function () {

            this.Area = [];
            for (var x = 0; x < numRows; x++) {
                this.Area[x] = [];
                for (var y = 0; y < numCols; y++) {
                    this.Area[x][y] = Empty;
                }
            }




            this.solidBlocks = game.add.physicsGroup();
            this.enemyBlocks = game.add.physicsGroup();

            //////// Oyunun sınırları
            for (var i = 0; i < numRows; i++) {
                for (var j = 0; j < numCols; j++) {
                    if (i == 0) {
                        var solidBrick = this.solidBlocks.create(i * 32, j * 32, 'solidBrick');
                        solidBrick.body.immovable = true;
                        this.Area[i][j] = Rock;
                    }
                    else if (i != (numRows - 1) && (j == 0 || j == (numCols - 1))) {
                        var solidBrick = this.solidBlocks.create(i * 32, j * 32, 'solidBrick');
                        solidBrick.body.immovable = true;
                        this.Area[i][j] = Rock;
                    }
                    else if (i == (numRows - 1)) {
                        var solidBrick = this.solidBlocks.create(i * 32, j * 32, 'solidBrick');
                        solidBrick.body.immovable = true;
                        this.Area[i][j] = Rock;
                    }
                }
            }
            //// kırılmayan blockları yerleştir
            for (var i = 0 ; i < numRows ; i++) {
                for (var j = 0; j < numCols; j++) {
                    if ((i % 2 == 0) && (j % 2 == 0)) // blocklar arasında birer boşluk olucak şekilde yerleştircek)
                    {
                        var solidBrick = this.solidBlocks.create(i * 32, j * 32, 'solidBrick');
                        solidBrick.body.immovable = true;
                        this.Area[i][j] = Rock;
                        //game.add.sprite(i * 32, j * 32, 'solidBrick')
                    }
                }
            }
            ///////// kapıyı koy
            while (true) {
                var rndValueX = game.rnd.integerInRange(0, numRows - 1);
                var rndValueY = game.rnd.integerInRange(0, numCols - 1);
                if (rndValueX >= 2 || rndValueY >= 2) { // playerin olduğu ilk boşluklara bomb power up koymasın
                    if (this.Area[rndValueX][rndValueY] == Empty) {
                        console.log("portal pos x: " + rndValueX + " pos y: " + rndValueY);
                        this.levelPortal = game.add.sprite(rndValueX * 32, rndValueY * 32, 'portal');
                        game.physics.enable(this.levelPortal);
                        this.Area[rndValueX][rndValueY] = PowerUp;
                        break;
                    }
                }
            }

            while (true) {
                var rndValueX = game.rnd.integerInRange(0, numRows - 1);
                //console.log("rnd x: " + rndValueX);
                var rndValueY = game.rnd.integerInRange(0, numCols - 1);
                //console.log("rnd y: " + rndValueY);
                if (rndValueX >= 2 || rndValueY >= 2) { // playerin olduğu ilk boşluklara bomb power up koymasın
                    if (this.Area[rndValueX][rndValueY] == Empty) {
                        console.log("bomb power up pos x: " + rndValueX + " pos y: " + rndValueY);
                        this.bombPowerUp = game.add.sprite(rndValueX * 32, rndValueY * 32, 'boomb');
                        game.physics.enable(this.bombPowerUp);
                        break;
                    }
                }
            }

            while (true) {
                var rndValueX = game.rnd.integerInRange(0, numRows - 1);
                //console.log("rnd x: " + rndValueX);
                var rndValueY = game.rnd.integerInRange(0, numCols - 1);
                //console.log("rnd y: " + rndValueY);
                if (rndValueX >= 2 || rndValueY >= 2) { // playerin olduğu ilk boşluklara flame power up koymasın
                    if (this.Area[rndValueX][rndValueY] == Empty) {
                        console.log("flame power up pos x: " + rndValueX + " pos y: " + rndValueY);
                        this.flamePowerUp = game.add.sprite(rndValueX * 32, rndValueY * 32, 'flaame');
                        game.physics.enable(this.flamePowerUp);
                        break;
                    }
                }
            }

            while (true) {
                var rndValueX = game.rnd.integerInRange(0, numRows - 1);
                //console.log("rnd x: " + rndValueX);
                var rndValueY = game.rnd.integerInRange(0, numCols - 1);
                //console.log("rnd y: " + rndValueY);
                if (rndValueX >= 2 || rndValueY >= 2) { // playerin olduğu ilk boşluklara speed power up koymasın
                    if (this.Area[rndValueX][rndValueY] == Empty) {
                        console.log("speed power up pos x: " + rndValueX + " pos y: " + rndValueY);
                        this.speedPowerUp = game.add.sprite(rndValueX * 32, rndValueY * 32, 'speed');
                        game.physics.enable(this.speedPowerUp);

                        break;
                    }
                }
            }
            /////// INITIALIZE EXPLODABLE BRICKS
            this.brickBloks = game.add.physicsGroup();
            // HIDE POWER UPS
            var brick = this.brickBloks.create(this.levelPortal.position.x, this.levelPortal.position.y, 'explodableBrick');
            brick.body.immovable = true;
            var brick = this.brickBloks.create(this.speedPowerUp.position.x, this.speedPowerUp.position.y, 'explodableBrick');
            brick.body.immovable = true;
            var brick = this.brickBloks.create(this.flamePowerUp.position.x, this.flamePowerUp.position.y, 'explodableBrick');
            brick.body.immovable = true;
            var brick = this.brickBloks.create(this.bombPowerUp.position.x, this.bombPowerUp.position.y, 'explodableBrick');
            brick.body.immovable = true;

            for (var i = 0; i < NUMBER_OF_BRICKS - 1;) {
                var rndValueX = game.rnd.integerInRange(0, numRows - 1);
                //console.log("rnd x: " + rndValueX);
                var rndValueY = game.rnd.integerInRange(0, numCols - 1);
                //console.log("rnd y: " + rndValueY);
                // playerin olduğu ilk boşluklara brick koymasın
                if ((rndValueX >= 3 || rndValueY >= 3) && (rndValueX <= 37 || rndValueY <= 17)) {
                    if (this.Area[rndValueX][rndValueY] == Empty) {
                        var brick = this.brickBloks.create(rndValueX * 32, rndValueY * 32, 'explodableBrick');
                        brick.body.immovable = true;
                        //bricl.Isexplodable = true;
                        //game.add.sprite(rndValueX * 32, rndValueY * 32, 'explodableBrick');
                        this.Area[rndValueX][rndValueY] = Brick;
                        i++;
                    }
                }
                //console.log("var i : " + i);
            }

            //// iniliza monsters ////////////
            var count = 0;
            while (true) {
                var rndValueX = game.rnd.integerInRange(0, numRows - 1);
                //console.log("rnd x: " + rndValueX);
                var rndValueY = game.rnd.integerInRange(0, numCols - 1);
                //console.log("rnd y: " + rndValueY);
                if (rndValueX >= 2 || rndValueY >= 2) { // playerin olduğu ilk boşluklara speed power up koymasın
                    if (this.Area[rndValueX][rndValueY] == Empty) {

                        var enemy = new GameObjects.InitializeEnemy();
                        enemy.init(stageNumber, rndValueX * 32, rndValueY * 32, this.enemyBlocks);
                        console.log("Monster");
                        count++;
                        if (count >= this.numberOfEnemies)
                            break;
                    }
                }
            }
            //////////////// load music /////////////////////////////////      
            this.gameMusic.playMainMusic();
            //// ilk oyun açıldığında Reload fonksiyonunda değişkenleri destroy etmesin diye
            /// sadece diğer stage'lere geçildiğinde değişkenler destroy olsun diye
            this.isInitialized = true;
        },
        playPowerUpMusic: function () {
            this.gameMusic.playPowerUpMusic();
        },
        playEndOfGameMusic: function () {
            this.gameMusic.playEnterPortalMusic();
        },
        playPlayerDeadMusic:function(){
            this.gameMusic.playDeadMusic();
        },
        GetEnemies: function () {
            return this.enemyBlocks;
        },
        GetArea: function () {
            return this.Area;
        },
        UpdateArea: function (x, y, type) {
            this.Area[x][y] = type;
        },
        ReloadMap: function (stageNumer) {
            if (this.isInitialized) {
                this.enemyBlocks.destroy();
                this.brickBloks.destroy();
                this.solidBlocks.destroy();
                this.levelPortal.kill();
                this.flamePowerUp.kill();
                this.speedPowerUp.kill();
                this.bombPowerUp.kill();
            }
            this.LoadDynamicItems();
        },
    };
});

GameObjects.Sounds = (function () {
    return {
        mainMusic: "",
        powerUpMusic: "",
        plantBombMusic: "",
        bombExplosionMusic: "",
        deadMusic:"",
        init: function () {
            this.mainMusic = game.add.audio("mainMusic", 0.5, true);
            this.mainMusic.override = true;

            this.powerUpMusic = game.add.audio("powerUpMusic");
            this.powerUpMusic.override = true;
            this.powerUpMusic.addMarker("getPowerup", 1, 13, 0.5, true);
            this.powerUpMusic.addMarker("enterPortal", 14, 4, 0.5, false);

            this.plantBombMusic = game.add.audio("plantBombMusic", 0.1, false);
            this.bombExplosionMusic = game.add.audio("explosionBombMusic", 0.1, false);

            this.deadMusic = game.add.audio("deadMusic", 0.1, false);
        },
        playMainMusic: function () {
            console.log("playing Main song");
            this.mainMusic.play();
        },
        stopMainMusic: function () {
            this.mainMusic.stop();
        },
        playPowerUpMusic: function () {
            console.log("playing Power up song");
            this.stopMainMusic();
            this.powerUpMusic.play("getPowerup");
        },
        playEnterPortalMusic: function () {
            console.log("playing Entering Portal song");
            this.stopMainMusic();
            this.powerUpMusic.play("enterPortal");
            if (!this.powerUpMusic.isPlaying) {
                this.playMainMusic();
            }
        },
        playPlantBombMusic: function () {
            console.log("playing Planting Bomb song");
            this.plantBombMusic.play();
        },
        playBombExplosionMusic: function () {
            console.log("playing bomb Explosion song");
            this.bombExplosionMusic.play();
        },
        playDeadMusic: function () {
            console.log("playing dead music");
            this.deadMusic.play();
        }
    };
});

GameObjects.InitializeEnemy = (function () {
    return {
        bFacingRight: "",
        level: "",
        sprite: "",
        enemySpeed: "",
        //enemyBlock: "",
        //numberOfEnemies:"",
        init: function (stageNumber, x, y, enemyBlocks) {
            this.bFacingRight = true;
            this.level = stageNumber;
            //this.numberOfEnemies = 6;
            //this.enemyBlock = game.add.physicsGroup();
            if (this.level == 1) {
                this.baloonEnemy(x, y, enemyBlocks);
            }
            else if (this.level >= 2) {
                this.onionEnemy(x, y, enemyBlocks);
            }
        },
        baloonEnemy: function (x, y, enemyBlocks) {
            this.enemySpeed = 50;

            var enemy = enemyBlocks.create(x, y, 'enemy01moves');
            enemy.animations.add('walkRL', [1, 2, 3]);
            enemy.animations.add('dead', [0]);
            enemy.animations.play('walkRL', 8, true);
            enemy.body.collideWorldBounds = true;

            this.turnRight(enemy);
        },
        onionEnemy: function (x, y, enemyBlocks) {
            this.enemySpeed = 60;

            var enemy = enemyBlocks.create(x, y, 'enemy02moves');
            enemy.animations.add('walkRL', [1, 2, 3]);
            enemy.animations.add('dead', [0]);
            enemy.animations.play('walkRL', 8, true);
            enemy.body.collideWorldBounds = true;

            this.turnRight(enemy);
        },
        turnRight: function (sprite) {
            //sprite.x++;
            if (!this.bFacingRight) {
                sprite.scale.x *= -1;
                this.bFacingRight = true;
            }
            sprite.body.velocity.x = this.enemySpeed;
            sprite.animations.play("walkRL", 16, false);
            console.log("right");
        },
        turnLeft: function () {
            //sprite.x--;
            if (bFacingRight) {
                this.sprite.scale.x *= -1;
                bFacingRight = false;
            }
            this.sprite.body.velocity.x = -1 * this.enemySpeed;
            this.sprite.animations.play("walkRL", 16, false);
            console.log("left");
        },
        turnUp: function () {
            this.sprite.body.velocity.y = -1 * this.enemySpeed;
            console.log("up");
        },
        turnDown: function () {
            this.sprite.body.velocity.y = this.enemySpeed;
            console.log("down");
        }
    };
});